import unittest
from calc import square

class TestCalcModule(unittest.TestCase):
    def test_square(self):
        self.assertEqual(square(5), 25)

    def test_types(self):
        sefl.assertRaises(TypeError, square, True)
        sefl.assertRaises(TypeError, square, "5.2")
