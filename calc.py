def square(n):
    if type(n) not in [int, float]:
        raise TypeError("n doit être entier ou décimal")
    return n*n