from flask import Flask
from calc import square


app = Flask(__name__)

@app.route("/square/<number>")
def getSquare(number):

    n= int(number)
    res = None

    try:
        n = float(number)
    except:
        return "Problème..."

    try:
        res = square(n)
    except:
        return "Problème..."

    return str(res)


app.run(host="0.0.0.0", port=8083)